# Bdd - Centre de loisirs



## Sujet

Un centre de loisirs organise sur l'année différentes activités (escalade, raquette, cirque, tricot, rock acrobatique, ...). On s'intéresse à la gestion de la participation des adhérents aux différentes activités organisées par le centre, dans le but d'établir des statistiques sur la pratique des activités, et pouvoir ainsi mieux les planifier et mieux connaître la population qui les pratiquent.

Les informations répertoriées sur les adhérents sont : 
 - un n°d'identification
 - leurs nom, adresse et âge. 

Pour chaque adhérent, on va enregistrer également la liste des séances d'activités auxquelles il participe. Les activités proposées au catalogue du
centre sont référencées par leur nom. 

Sur chaque activité, on évalue le coût unitaire d'organisation d'une séance par client, et le prix unitaire de vente à un client de la participation à une séance.
On suppose que, pour une activité donnée, les coût et prix unitaires sont fixés indépendamment des séances. Pour chaque séance d'activité, on veut pouvoir retrouver ses dates et heures d'organisation, ainsi qu'une note d'appréciation attribuée individuellement par chaque client qui s'y est inscrit.


## Modélisation de la base de données

 - 1.1 Réaliser le dictionnaire de données
 - 1.2 Réaliser le Modèle Entité Association
 - 1.3 Réaliser le Modèle Physique de Données
 - 1.4 Exporter le fichier SQL généré
 - 1.5 Ajouter un compte utilisateur restreint 